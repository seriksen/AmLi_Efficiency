#include "OD_Efficiency.h"

#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

OD_Efficiency::OD_Efficiency() : Analysis() {

  // Setup config service
  m_conf = ConfigSvc::Instance();

  // Load ROOT branches
  m_event->IncludeBranch("pulsesTPC");
  m_event->IncludeBranch("eventTPC");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("ms");
  m_event->IncludeBranch("other");
  m_event->IncludeBranch("pileUp");
  m_event->IncludeBranch("kr83m");

  m_event->Initialize();
  m_cuts->sr1()->Initialize();

  selector = new Selector(m_event, m_conf);
  histograms = new Histograms(m_event, m_conf, m_hists);

}

OD_Efficiency::~OD_Efficiency() {
}

void OD_Efficiency::Initialize() { INFO("Initializing OD_Efficiency Analysis"); }

void OD_Efficiency::Execute() {

  int trigger_result{selector->Check_Trigger()};
  std::string base_path{"All_Triggers"};



  od_pulses_all = selector->OD_Get_Pulse_IDs();
  od_pulses_coincidence5 = selector->OD_Get_Pulse_IDs_Coincidence_Cut(od_pulses_all);
  od_pulses_noise = selector->OD_Get_Pulse_IDs_Noise_Cut(od_pulses_coincidence5);
  od_pulses_100kev = selector->OD_Get_Pulse_IDs_100keV(od_pulses_noise);
  od_pulses_200kev = selector->OD_Get_Pulse_IDs_200keV(od_pulses_100kev);

  largest_od_pulse_id = selector->OD_Get_Largest_Pulse_ID(od_pulses_noise);

  histograms->Histogram_OD_Pulses(base_path + "/OD/All_Pulses",
                                  od_pulses_all);
  histograms->Histogram_OD_Pulses(base_path + "/OD/Noise_Cut",
                                  od_pulses_noise);
  histograms->Histogram_OD_Pulses(base_path + "/OD/100keV",
                                  od_pulses_100kev);
  histograms->Histogram_OD_Pulses(base_path + "/OD/200keV",
                                  od_pulses_200kev);
  histograms->Histogram_OD_Pulses(base_path + "/OD/Largest",
                                  largest_od_pulse_id);

  skin_pulses_all = selector->Skin_Get_Pulse_IDs();
  skin_pulses_noise = selector->Skin_Get_Pulse_IDs_Noise_Cut(skin_pulses_all);
  skin_pulses_100kev = selector->Skin_Get_Pulse_IDs_100keV(skin_pulses_noise);


  int i{0};
  histograms->NEvents(i);
  if ((*m_event->m_singleScatter)->nSingleScatters > 0) {
    base_path = std::to_string(trigger_result) + "/SS";
    base_path = "All_Triggers/SS";
    i += 1;
    Process_Cut(base_path, i);
    Process_Capture_Time(base_path);

    int s2_id{(*m_event->m_singleScatter)->s2PulseID};
    int s1_id{(*m_event->m_singleScatter)->s1PulseID};

    float s1Area{(*m_event->m_singleScatter)->correctedS1Area_phd};
    float s2Area{(*m_event->m_singleScatter)->correctedS2Area_phd};

    // m_cuts->sr1()->PassesETrainVeto(s2_id, "S2")

    if (m_cuts->sr1()->TPCMuonVeto() &&
        m_cuts->sr1()->BufferStartTime() && m_cuts->sr1()->BufferStopTime(s1_id,s2_id) &&
        m_cuts->sr1()->PassExcessArea() &&
        m_cuts->sr1()->PassODBurstNoiseTagSS(s1_id, s2_id))
        ) {

      base_path += "/Livetime";
      i += 1;
      Process_Cut(base_path, i);

      if (m_cuts->sr1()->S2Width_SS() &&
          m_cuts->sr1()->NarrowS2Cut() &&
          m_cuts->sr1()->S2EarlyPeakCut() &&
          m_cuts->sr1()->IsValidXY(s2_id) &&
          m_cuts->sr1()->GasS2TBACut()
          ) {
        base_path += "/S2_Cuts";
        i += 1;
        Process_Cut(base_path, i);

        if (m_cuts->sr1()->ProminenceCut_SS() &&
            m_cuts->sr1()->Stinger(s1_id) &&
            m_cuts->sr1()->S1TBA_SS() &&
            m_cuts->sr1()->PassHSCCut(s1_id) &&
            m_cuts->sr1()->PassS1ShapeCut(s1_id) &&
            m_cuts->sr1()->PassS1ChannelTiming(s1_id)) {
          base_path += "/S1_Cuts";
          i += 1;
          Process_Cut(base_path, i);

          if (selector->FID()) {
            base_path += "/Fiducial";
            i += 1;
            Process_Cut(base_path, i);


            float sigma{
                m_event->CalculateNRBandDistance(s1Area, log10(s2Area))};
            if (sigma >= -1 && sigma <= 1) {
              base_path += "/NR_Band";
              i += 1;
              Process_Cut(base_path, i);
              Process_Capture_Time(base_path);
            }
            }
            }
          }
        }
      }
  return;
}


void OD_Efficiency::Process_Capture_Time(std::string base_path) {

  float start_time;
  float phd;
  float tpc_time{(*m_event->m_tpcPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID]};

  for (auto &&id : od_pulses_noise) {
    start_time = (*m_event->m_odHGPulses)->pulseStartTime_ns[id];
    phd = (*m_event->m_odHGPulses)->pulseArea_phd[id];

    histograms->Capture_Time(base_path + "/CaptureTime/Noise",
                             abs(start_time - tpc_time) / 1000,
                             phd);
  }
  if (largest_od_pulse_id > -1) {
    start_time = (*m_event->m_odHGPulses)->pulseStartTime_ns[largest_od_pulse_id];
    phd = (*m_event->m_odHGPulses)->pulseArea_phd[largest_od_pulse_id];
    histograms->Capture_Time(base_path + "/CaptureTime/Noise",
                             abs(start_time - tpc_time) / 1000,
                             phd);
  }
  return;
}


void OD_Efficiency::Process_Cut(std::string base_path,
                                int i) {
  histograms->NEvents(i);

  histograms->Histogram_TPC(base_path + "/Detectors/TPC");
  histograms->Histogram_OD_Pulses(base_path + "/OD/coincidence5",
                                  od_pulses_coincidence5);
  histograms->Histogram_Skin_Pulses(base_path + "/Detectors/Skin/100keV",
                                    skin_pulses_100kev);
  //histograms->OD_Positions(base_path + "/OD_Positions",
  //                         od_pulses_noise);


  Process_Veto_With_Prompt_Cut(base_path + "/Veto/Prompt_5coinc/0kev",
                                        od_pulses_noise,
                                        skin_pulses_100kev,
                                        od_pulses_coincidence5);
  Process_Veto_With_Prompt_Cut(base_path + "/Veto/Prompt_5coinc/100kev",
                                        od_pulses_100kev,
                                        skin_pulses_100kev,
                                        od_pulses_coincidence5);
  Process_Veto_With_Prompt_Cut(base_path + "/Veto/Prompt_5coinc/200kev",
                                        od_pulses_200kev,
                                        skin_pulses_100kev,
                                        od_pulses_coincidence5);

  Process_Veto_No_Prompt(base_path + "/Veto/No_Prompt/0keV",
                         od_pulses_noise,
                         skin_pulses_100kev);
  Process_Veto_No_Prompt(base_path + "/Veto/No_Prompt/100kev",
                         od_pulses_100kev,
                         skin_pulses_100kev);
  Process_Veto_No_Prompt(base_path + "/Veto/No_Prompt/200kev",
                         od_pulses_200kev,
                         skin_pulses_100kev);
  return;
}

void OD_Efficiency::Finalize() {
  INFO("Finalizing OD_Efficiency Analysis");
}

void OD_Efficiency::Process_Veto_With_Prompt_Cut(std::string base_path,
                                                 std::vector<int> od_pulses,
                                                 std::vector<int> skin_pulses,
                                                 std::vector<int> potential_prompt_pulses) {

  bool od_veto{false};
  double od_time{100000000000000};
  int veto_pulse;
  double prev_od_time{10000000000000000};
  int largest_pulse_id;
  if (od_pulses.size() > 0) {
    largest_pulse_id = od_pulses[0];
  }
  bool skin_veto{false};
  double skin_time{1000000000000000000};
  double prev_skin_time{10000000000000000};

  double tpc_time{(*m_event->m_tpcPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID]};

  bool prompt_veto{false};
  float prompt_time;
  for (auto &&id : potential_prompt_pulses) {
    prompt_time = (*m_event->m_odHGPulses)->pulseStartTime_ns[id];
    if (abs(prompt_time - tpc_time) < 400) {
      prompt_veto = true;
    }
  }

  if (!prompt_veto) {
    tpc_time -= 400;
    for (auto &&id : od_pulses) {
      // check if within veto window
      od_time = (*m_event->m_odHGPulses)->pulseStartTime_ns[id];
      if (od_time > tpc_time) {
        if ((*m_event->m_odHGPulses)->pulseArea_phd[id] >
            (*m_event->m_odHGPulses)->pulseArea_phd[largest_pulse_id]) {
          largest_pulse_id = id;
        }
        if (od_time < prev_od_time) {
          od_veto = true;
          veto_pulse = id;
          prev_od_time = od_time;
        }
      }
    }
    if (od_pulses.size() > 0) {
      histograms->OD_Neutron_Capture_Times(
          base_path,
          (*m_event->m_odHGPulses)->pulseStartTime_ns[largest_pulse_id] -
              tpc_time);
    }

    for (auto &&id : skin_pulses) {
      // check if within veto window
      skin_time = (*m_event->m_skinPulses)->pulseStartTime_ns[id];
      if (skin_time > tpc_time && skin_time < prev_skin_time) {
        skin_veto = true;
        prev_skin_time = skin_time;
      }
    }

    if (od_veto) {
      histograms->Veto_Time_Difference(base_path + "/Veto_OD",
                                       od_time - tpc_time);
      histograms->Histogram_OD_Pulses(base_path + "/Veto_OD", veto_pulse);
      // histograms->OD_Positions(base_path + "/Veto_OD", veto_pulse);
    }
    if (skin_veto) {
      histograms->Veto_Time_Difference(base_path + "/Veto_Skin",
                                       skin_time - tpc_time);
    }
    if (od_veto || skin_veto) {
      if (skin_time < od_time) {
        histograms->Veto_Time_Difference(base_path + "/Veto_All",
                                         skin_time - tpc_time);
      } else {
        histograms->Veto_Time_Difference(base_path + "/Veto_All",
                                         od_time - tpc_time);
      }
    }
  }
  else {
    histograms->Histogram_OD_Pulses(base_path + "/Veto_OD", 1.);
    histograms->Veto_Time_Difference(base_path + "/Veto_All", 1.);
  }
  return;
}


void OD_Efficiency::Process_Veto_No_Prompt(std::string base_path,
                                           std::vector<int> od_pulses,
                                           std::vector<int> skin_pulses) {

  bool od_veto{false};
  double od_time{100000000000000};
  int veto_pulse;
  double prev_od_time{10000000000000000};
  int largest_pulse_id;
  if (od_pulses.size() > 0) {
    largest_pulse_id = od_pulses[0];
  }
  bool skin_veto{false};
  double skin_time{1000000000000000000};
  double prev_skin_time{10000000000000000};

  double tpc_time{(*m_event->m_tpcPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID] - 400};

  for (auto &&id : od_pulses) {
    // check if within veto window
    od_time = (*m_event->m_odHGPulses)->pulseStartTime_ns[id];
    if (od_time > tpc_time) {
      if ((*m_event->m_odHGPulses)->pulseArea_phd[id] > (*m_event->m_odHGPulses)->pulseArea_phd[largest_pulse_id]) {
        largest_pulse_id = id;
      }
      if (od_time < prev_od_time) {
        od_veto = true;
        veto_pulse = id;
        prev_od_time = od_time;
      }
    }
  }
  if (od_pulses.size() > 0) {
    histograms->OD_Neutron_Capture_Times(base_path, (*m_event->m_odHGPulses)->pulseStartTime_ns[largest_pulse_id] - tpc_time);
  }

  for (auto &&id : skin_pulses) {
    // check if within veto window
    skin_time = (*m_event->m_skinPulses)->pulseStartTime_ns[id];
    if (skin_time > tpc_time && skin_time < prev_skin_time) {
      skin_veto = true;
      prev_skin_time = skin_time;
    }
  }

  if (od_veto){
    histograms->Veto_Time_Difference(base_path + "/Veto_OD", od_time - tpc_time);
    histograms->Histogram_OD_Pulses(base_path + "/Veto_OD", veto_pulse);
    //histograms->OD_Positions(base_path + "/Veto_OD", veto_pulse);
  }
  if (skin_veto) {
    histograms->Veto_Time_Difference(base_path + "/Veto_Skin", skin_time - tpc_time);
  }
  if (od_veto || skin_veto) {
    if (skin_time < od_time) {
      histograms->Veto_Time_Difference(base_path + "/Veto_All", skin_time - tpc_time);
    }
    else {
      histograms->Veto_Time_Difference(base_path + "/Veto_All", od_time - tpc_time);
    }
  }
  return;
}