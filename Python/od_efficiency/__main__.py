
import multiprocessing as mp
import glob
import functools
import tqdm
import ROOT
from . import background_baccarat_root
from . import neutron_capture_time
import os

sources = ['tpc_neutrons_100kev', 'tpc_neutrons_1mev', 'tpc_neutrons_6mev']

for source in sources:
    files = glob.glob(
        '/hdfs/user/ak18773/od_simulations/Neutron_Efficency/Energy_Only/BACCARAT_6.2.14_DER_9.1.0_LZAP_5.4.1'
        '/{0}/baccarat/*.root'.format(source))

    print('N files:', len(files))

    out_dir = 'outputs/amli_capture_times/'#{0}/'.format(source)
    os.mkdir(out_dir)

    with mp.Pool(30) as pool:
        r = list(tqdm.tqdm(
            #pool.imap(functools.partial(background_baccarat_root.process_file, outdir=out_dir), files)))
            pool.imap(functools.partial(neutron_capture_time.process_file, outdir=out_dir), files)))


