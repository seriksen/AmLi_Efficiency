"""
Code for calculating the detection efficiency in simulations using energy depositions


Efficiency = numerator / denominator

numerator = energy deposits above threshold within veto window in the OD or the event is vetoes by the Skin

denominator = single scatters and energy deposit in ROI and fiducial volume
"""

import uproot as up

import numpy as np
import awkward as ak
import ROOT
import os


def process_file(file_name, outdir='./'):
    outfile_name = file_name[file_name.rfind('/') + 1:file_name.rfind('.root')] + '_efficiency.root'

    if os.path.exists(outdir + outfile_name):
        return

    events = up.open(file_name)['Events']

    x = events['deposits.positions_x_mm'].array()
    y = events['deposits.positions_y_mm'].array()
    z = events['deposits.positions_z_mm'].array()
    e_deps = events['deposits.EnergyDeps_keV'].array()
    vols = events['deposits.volumeNames'].array()
    times = events['deposits.times_ns'].array()
    pids = events['deposits.pids'].array()

    # Overview plots
    h_gdls_gd157capture_time = ROOT.TH1F("GdLS_Gd157CaptureTime_All", "Neutron capture time", 2000, 0, 2000)
    h_gdls_gd157capture_time_ss = ROOT.TH1F("GdLS_Gd157CaptureTime_SS", "Neutron capture time", 2000, 0, 2000)

    h_gdls_gd155capture_time = ROOT.TH1F("GdLS_Gd155CaptureTime_All", "Neutron capture time", 2000, 0, 2000)
    h_gdls_gd155capture_time_ss = ROOT.TH1F("GdLS_Gd155CaptureTime_SS", "Neutron capture time", 2000, 0, 2000)

    h_gdls_hcapture_time = ROOT.TH1F("GdLS_H_CaptureTime_All", "Neutron capture time", 2000, 0, 2000)
    h_gdls_hcapture_time_ss = ROOT.TH1F("GdLS_H_CaptureTime_SS", "Neutron capture time", 2000, 0, 2000)

    h_all_hcapture_time = ROOT.TH1F("all_H_CaptureTime_All", "Neutron capture time", 2000, 0, 2000)
    h_all_hcapture_time_ss = ROOT.TH1F("all_H_CaptureTime_SS", "Neutron capture time", 2000, 0, 2000)


    # Loop over files
    for i in range(len(e_deps)):
        if len(vols[i]) > 0:
            # Get this event
            d_x = x[i] / 10
            d_y = y[i] / 10
            d_z = z[i] / 10
            d_e = e_deps[i]
            d_vol = vols[i]
            d_time = times[i]

            # neutron capture time
            pid = pids[i]
            for j, p in enumerate(pid):
                if p == 1000641580:
                    h_gdls_gd157capture_time.Fill(d_time[j] / 1000)
                elif p == 1000641560:
                    h_gdls_gd155capture_time.Fill(d_time[j] / 1000)
                elif p == 1000010020:
                    h_all_hcapture_time.Fill(d_time[j] / 1000)
                    if d_vol[j] == 'ScintillatorCenter':
                        h_gdls_hcapture_time.Fill(d_time[j] / 1000)

            # Where are the deposits
            lxt = np.where(d_vol == 'LiquidXenonTarget')
            scint = np.where(d_vol == 'ScintillatorCenter')

            #####################
            ## Single Scatters ##
            #####################
            if np.size(lxt) > 0:
                lx_x = d_x[lxt]
                lx_y = d_y[lxt]
                lx_z = d_z[lxt]
                lx_e = d_e[lxt]
                lx_time = d_time[lxt][0]
                lx_r = np.sqrt(lx_x ** 2 + lx_y ** 2)
                lx_e_tot = sum(lx_e)
                is_ss, r_sigma, z_sigma = ss_cut(lx_r, lx_z, lx_e)

                # If Single Scatter
                if is_ss:

                    for j, p in enumerate(pid):
                        if p == 1000641580:
                            h_gdls_gd157capture_time_ss.Fill(d_time[j] / 1000)
                        elif p == 1000641560:
                            h_gdls_gd155capture_time_ss.Fill(d_time[j] / 1000)
                        elif p == 1000010020:
                            h_all_hcapture_time_ss.Fill(d_time[j] / 1000)
                            if d_vol[j] == 'ScintillatorCenter':
                                h_gdls_hcapture_time_ss.Fill(d_time[j] / 1000)


    # write histograms
    outfile_name = file_name[file_name.rfind('/') + 1:file_name.rfind('.root')] + '_capture_time.root'
    tfile = ROOT.TFile(outdir + outfile_name, "RECREATE")

    # Overview plots
    h_gdls_gd157capture_time.Write()
    h_gdls_gd157capture_time_ss.Write()

    h_gdls_gd155capture_time.Write()
    h_gdls_gd155capture_time_ss.Write()

    h_gdls_hcapture_time.Write()
    h_gdls_hcapture_time_ss.Write()

    h_all_hcapture_time.Write()
    h_all_hcapture_time_ss.Write()

    tfile.Close()


def ss_cut(rs, zs, energies):
    """
    Determine if deposits are close enough to be called a signal scatter

    :param rs: deposit X locations
    :type rs: ak.Array[float] or float
    :param zs: deposit Z locations
    :type zs: ak.Array[float] or float
    :param energies: deposit energies
    :type energies: ak.Array[float] or float
    :return: signel scatter result, r sigma, z sigma
    :rtype: list[bool, float, float]
    """

    r_sigma = energy_weighted_sigma(rs, energies)

    z_sigma = energy_weighted_sigma(zs, energies)

    if r_sigma < 3.0 and z_sigma < 0.2:
        result = True
    else:
        result = False
    return result, r_sigma, z_sigma


def energy_weighted_mean(values, energies):
    """
    Weight a value based upon the energy

    :param values:
    :type values: float, ak.Array[float]
    :param energies:
    :type energies: float, ak.Array[float]
    :return:
    :rtype: float
    """

    return sum(values * energies) / sum(energies)


def energy_weighted_sigma(values, energies):
    """

    :param values:
    :type values:
    :param energies:
    :type energies:
    :return:
    :rtype:
    """
    if len(values) == 1:
        sigma = 0.0
    else:
        weighted_mean = energy_weighted_mean(values, energies)
        energy_sq = sum(energies * energies)
        distance = sum(((values - weighted_mean) ** 2) * energies)
        total_e = sum(energies)
        sigma = np.sqrt(distance * total_e / (total_e ** 2 - energy_sq))

    return sigma


def wimp_roi_cut(energy):
    """
    Apply WIMP ROI cut: 6keV - 30keV

    :param energy:
    :type energy: float
    :return:
    :rtype: bool
    """
    if 6.0 < energy < 30.0:
        return True
    else:
        return False


def fiducial_cut(r, z):
    """
    Apply Fiducial cut.
    Simple Rectangle to match TDR era

    :param r: scatter r position in cm
    :type r: float
    :param z: scatter z position in cm
    :type z: float
    :return: True if r and z are in the fiducial region
    :rtype: bool
    """

    if r < 68.8 and 1.5 < z < 132.1:
        return True
    else:
        return False